<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'format_institutes', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   format_institutes
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['currentsection'] = 'This topic';
$string['editsection'] = 'Edit topic';
$string['editsectionname'] = 'Edit topic name';
$string['deletesection'] = 'Delete topic';
$string['newsectionname'] = 'New name for topic {$a}';
$string['sectionname'] = 'Topic';
$string['pluginname'] = 'Institutes format';
$string['section0name'] = 'General';
$string['page-course-view-institutes'] = 'Any course main page in institutes format';
$string['page-course-view-institutes-x'] = 'Any course page in institutes format';
$string['hidefromothers'] = 'Hide topic';
$string['showfromothers'] = 'Show topic';
$string['rootsections'] = 'Root sections';
$string['thumbnailfile'] = 'Course image/video file';
$string['completed'] = 'Completed';
$string['schedule'] = 'Schedule';
$string['schedulefile'] = 'Schedule File';
$string['infotext'] = 'Embed Video';
$string['pickup'] = 'Resume Course';
$string['schedule'] = 'Schedule';
$string['gradebook'] = 'Gradebook';
$string['activity'] = 'Activity';
$string['requiredasset'] = 'Required asset';
$string['status'] = 'Status';
$string['view'] = 'View';
$string['take'] = 'Take';
$string['viewmodule'] = 'View Module';
$string['viewwebsite'] = 'View Website';
$string['viewlink'] = 'View Link';
$string['notstarted'] = 'Not Started';
$string['completed'] = 'Completed';
$string['notcompleted'] = 'Not Completed';
$string['inprogress'] = 'In Progress';
$string['markcomplete'] = 'Mark Complete';
$string['markincomplete'] = 'Mark Incomplete';
$string['fail'] = 'Fail';
$string['createnewsection'] = 'Create new section';
$string['addsection'] = 'Add section';
$string['coursemenusettings'] = 'Course menu settings';
$string['glossary'] = 'Glossary';
$string['faq'] = 'FAQ';
$string['save'] = 'Save';
$string['selectoption'] = ' -- Select {$a} -- ';
$string['resources'] = 'Resources';
$string['name'] = 'Name';
$string['section'] = 'Section';
$string['type'] = 'Type';
$string['actions'] = 'Actions';
$string['resourcessettings'] = 'Resources Settings';
$string['nothingtodisplay'] = 'Nothing to display';
$string['editcategory'] = 'Edit category';
$string['createcategory'] = 'Create category';
$string['deletecategory'] = 'Delete category';
$string['confirmcategorydelete'] = 'Are you sure want to delete category <b>{$a}</b>?';
$string['download'] = 'Download';
$string['watchvideo'] = 'Watch video';
$string['displayonresources'] = 'Display on Resources Page';
$string['displayoncourse'] = 'Display on Course Page';
