<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * institutes version file.
 *
 * @package    format_institutes
 * @author     institutes
 * @copyright  2016 sebale, sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../../config.php');
require_once('lib.php');

$systemcontext   = context_system::instance();
require_capability('format/institutes:manageresources', $systemcontext);

$id     = required_param('id', PARAM_INT); // Option id.
$search = optional_param('search', '', PARAM_RAW);
$action = optional_param('action', '', PARAM_RAW);
$show   = optional_param('show', 0, PARAM_INT);
$hide   = optional_param('hide', 0, PARAM_INT);
$courseshow   = optional_param('courseshow', 0, PARAM_INT);
$coursehide   = optional_param('coursehide', 0, PARAM_INT);

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);
$sections = format_get_course_sections($course);

require_login($course);

if ($show){
    
    $record = $DB->get_record('course_format_resources', array('cmid'=>$show, 'courseid'=>$course->id, 'type'=>'module'));
    if ($record){
        $record->state = 1;
        $record->id = $DB->update_record('course_format_resources', $record);
    } else {
        $record = new stdClass();
        $record->cmid = $show;
        $record->courseid = $course->id;
        $record->type = 'module';
        $record->sortorder = 0;
        $record->state = 1;
        $record->id = $DB->insert_record('course_format_resources', $record);
        
        $resources = $DB->get_records_sql("SELECT id, sortorder FROM {course_format_resources} WHERE id != $record->id AND courseid = :courseid AND type = :type ORDER BY sortorder", array('courseid'=>$course->id, 'type'=>'module'));

        $i = 1;
        foreach ($resources as $resource){
            $resource->sortorder = $i++;
            $DB->update_record("course_format_resources", $resource);
        }    
    }
    
    redirect(new moodle_url('/course/format/institutes/resourcessettings.php', array('id'=>$course->id)));

} elseif ($hide){
    
    $record = $DB->get_record('course_format_resources', array('cmid'=>$hide, 'courseid'=>$course->id, 'type'=>'module'));
    if ($record){
        $record->state = 0;
        $record->id = $DB->update_record('course_format_resources', $record);
    } else {
        $record = new stdClass();
        $record->cmid = $show;
        $record->courseid = $course->id;
        $record->type = 'module';
        $record->sortorder = 0;
        $record->state = 0;
        $record->id = $DB->insert_record('course_format_resources', $record);
        
        $resources = $DB->get_records_sql("SELECT id, sortorder FROM {course_format_resources} WHERE id != $record->id AND courseid = :courseid AND type = :type ORDER BY sortorder", array('courseid'=>$course->id, 'type'=>'module'));

        $i = 1;
        foreach ($resources as $resource){
            $resource->sortorder = $i++;
            $DB->update_record("course_format_resources", $resource);
        }
    }
    
    redirect(new moodle_url('/course/format/institutes/resourcessettings.php', array('id'=>$course->id)));

} elseif ($courseshow){
    
    $record = $DB->get_record('course_format_resources', array('cmid'=>$courseshow, 'courseid'=>$course->id, 'type'=>'module'));
    if ($record){
        $record->coursestate = 1;
        $record->id = $DB->update_record('course_format_resources', $record);
    } else {
        $record = new stdClass();
        $record->cmid = $courseshow;
        $record->courseid = $course->id;
        $record->type = 'module';
        $record->sortorder = 0;
        $record->state = 0;
        $record->coursestate = 1;
        $record->id = $DB->insert_record('course_format_resources', $record);
        
        $resources = $DB->get_records_sql("SELECT id, sortorder FROM {course_format_resources} WHERE id != $record->id AND courseid = :courseid AND type = :type ORDER BY sortorder", array('courseid'=>$course->id, 'type'=>'module'));

        $i = 1;
        foreach ($resources as $resource){
            $resource->sortorder = $i++;
            $DB->update_record("course_format_resources", $resource);
        }
    }
    
    redirect(new moodle_url('/course/format/institutes/resourcessettings.php', array('id'=>$course->id)));
    
} elseif ($coursehide){
    
    $record = $DB->get_record('course_format_resources', array('cmid'=>$coursehide, 'courseid'=>$course->id, 'type'=>'module'));
    if ($record){
        $record->coursestate = 0;
        $record->id = $DB->update_record('course_format_resources', $record);
    } else {
        $record = new stdClass();
        $record->cmid = $coursehide;
        $record->courseid = $course->id;
        $record->type = 'module';
        $record->sortorder = 0;
        $record->state = 0;
        $record->coursestate = 0;
        $record->id = $DB->insert_record('course_format_resources', $record);
        
        $resources = $DB->get_records_sql("SELECT id, sortorder FROM {course_format_resources} WHERE id != $record->id AND courseid = :courseid AND type = :type ORDER BY sortorder", array('courseid'=>$course->id, 'type'=>'module'));

        $i = 1;
        foreach ($resources as $resource){
            $resource->sortorder = $i++;
            $DB->update_record("course_format_resources", $resource);
        }
    }
    
    redirect(new moodle_url('/course/format/institutes/resourcessettings.php', array('id'=>$course->id)));
}

$records = array();
$allrecords = $DB->get_records_sql('SELECT id, cmid, state, coursestate FROM {course_format_resources} WHERE courseid = :courseid AND type = :type', array('courseid'=>$course->id, 'type'=>'module'));
if (count($allrecords)){
    foreach ($allrecords as $record){
        $records[$record->cmid] = $record;
    }
}


$PAGE->set_context($context);
$PAGE->set_pagelayout('course');
$pageparams = array('id' => $id);
$PAGE->set_url('/course/format/institutes/resourcessettings.php', $pageparams);

$title = get_string('resourcessettings', 'format_institutes');

$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$data = array(); 
//$resources = array('book', 'resource', 'folder', 'imscp', 'page', 'url');
$resources = array('resource');
$modinfo = get_fast_modinfo($course);
foreach ($modinfo->cms as $modid=>$mod) {
    if (!in_array($mod->modname, $resources)) continue;
    
    $line = array();
    // name
    if (isset($mod->url)){
        $line[] = html_writer::link($mod->url, $mod->get_formatted_name(), array('title' => $mod->modfullname));    
    } else {
        $line[] = $mod->get_formatted_name();
    }
    
    // type
    $line[] = get_string('pluginname', 'mod_'.$mod->modname);
    
    // section
    $line[] = get_section_name($course, $sections[$mod->section]);
    
    // displayonresources
    $buttons = array();
    $urlparams = array('id' => $course->id);
    $showhideurl = new moodle_url('/course/format/institutes/resourcessettings.php', $urlparams);
    if (isset($records[$mod->id]) and $records[$mod->id]->state > 0) {
        $showhideurl->param('hide', $mod->id);
        $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/hide'), 'alt' => get_string('hide'), 'class' => 'iconsmall'));
        $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('hide')));
    } else {
        $showhideurl->param('show', $mod->id);
        $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/show'), 'alt' => get_string('show'), 'class' => 'iconsmall'));
        $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('show')));
    }
    $line[] = implode(' ', $buttons);
    
    if ($sections[$mod->section]->section > 0){
        $buttons = array();
        $urlparams = array('id' => $course->id);
        $showhideurl = new moodle_url('/course/format/institutes/resourcessettings.php', $urlparams);
        if (!isset($records[$mod->id]) or (isset($records[$mod->id]) and $records[$mod->id]->coursestate > 0)) {
            $showhideurl->param('coursehide', $mod->id);
            $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/hide'), 'alt' => get_string('hide'), 'class' => 'iconsmall'));
            $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('hide')));
        } else {
            $showhideurl->param('courseshow', $mod->id);
            $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/show'), 'alt' => get_string('show'), 'class' => 'iconsmall'));
            $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('show')));
        }
        $line[] = implode(' ', $buttons);
    } else {
        $line[] = '';
    }
    
    $data[] = $row = new html_table_row($line);
}

if (count($data)){
    $table = new html_table();
    $table->head  = array(get_string('name', 'format_institutes'), get_string('type', 'format_institutes'), get_string('section', 'format_institutes'), get_string('displayonresources', 'format_institutes'), get_string('displayoncourse', 'format_institutes'));
    $table->colclasses = array('leftalign name', 'leftalign section', 'leftalign type', 'centeralign actions', 'centeralign actions');

    $table->id = 'resourcessettings';
    $table->attributes['class'] = 'generaltable';
    $table->data  = $data;

    echo html_writer::table($table);
} else {
    echo html_writer::tag('div', get_string('nothingtodisplay', 'format_institutes'), array('class'=>'alert alert-success'));
}

echo $OUTPUT->footer();
