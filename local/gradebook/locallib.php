<?php

function local_gradebook_display($course){
    global $CFG, $USER;
    
    $output = '';
    
    $course = course_get_format($course)->get_course();
    
    $modinfo = get_fast_modinfo($course);
    $sections = local_gradebook_get_course_sections($course);
    
    if (count($sections)){
        $course_quizzes = local_gradebook_get_course_quizzes($course);
        
        // level1 sections start
        $j = 1;
        $output .= html_writer::start_tag('ul', array('class'=>'course-level1-sections'));
        foreach ($sections[0] as $section1=>$thissection1){
            
            $output .= html_writer::start_tag('li', array('class'=>'course-section-box'));
            $toggler = html_writer::tag('i', '', array('class'=>'toggler ion-ios-arrow-right'));
            $counter = html_writer::tag('span', ((strlen($j) > 1) ? $j : '0'.$j), array('class'=>'section-counter'));
            $sectionname = $toggler.$counter.get_section_name($course, $thissection1);
            
            $output .= html_writer::tag('div', $sectionname, array('class'=>'section-header'));
            
            // level2 sections start
            if (isset($sections[$thissection1->id]) and count($sections[$thissection1->id])){
                $output .= html_writer::start_tag('div', array('class'=>'course-level2-sections'));
                
                    $output .= html_writer::start_tag('table', array('class'=>'course-level2-sections'));
                        $output .= html_writer::start_tag('thead');
                            $output .= html_writer::start_tag('tr');
                                $output .= html_writer::tag('th', get_string('quizname', 'local_gradebook'), array('class'=>'name'));
                                $output .= html_writer::tag('th', get_string('status', 'local_gradebook'), array('class'=>'hidden_mobile'));
                                $output .= html_writer::tag('th', get_string('attempts', 'local_gradebook'), array('class'=>'hidden_mobile'));
                                $output .= html_writer::tag('th', get_string('grade', 'local_gradebook'), array('class'=>'grade'));
                            $output .= html_writer::end_tag('tr');
                        $output .= html_writer::end_tag('thead');
                    
                    foreach ($sections[$thissection1->id] as $section2=>$thissection2){
                        $output .= html_writer::start_tag('tbody');
                        
                            $output .= html_writer::start_tag('tr');
                            $output .= html_writer::tag('td', get_section_name($course, $thissection2), array('class'=>'section-level2', 'colspan'=>4));
                            $output .= html_writer::end_tag('tr');
                        
                            // level3 sections start
                            if (isset($sections[$thissection2->id]) and count($sections[$thissection2->id])){
                                foreach ($sections[$thissection2->id] as $section3=>$thissection3){
                                    $output .= html_writer::start_tag('tr');
                                    $output .= html_writer::tag('td', get_section_name($course, $thissection3), array('class'=>'section-level3', 'colspan'=>4));
                                    $output .= html_writer::end_tag('tr');
                                    
                                    // level4 sections start
                                    if (isset($sections[$thissection3->id]) and count($sections[$thissection3->id])){
                                        foreach ($sections[$thissection3->id] as $section4=>$thissection4){
                                            $output .= local_gradebook_get_quizzes_list($course, $thissection4, $course_quizzes);
                                        }
                                    }
                                    // level4 sections end
                                }
                            }
                            // level3 sections end
                        
                        $output .= html_writer::end_tag('tbody');            
                    }
                
                    $output .= html_writer::end_tag('table');
                $output .= html_writer::end_tag('div');
            }
            // level2 sections end

            $output .= html_writer::end_tag('li');            
            $j++;
        }
        // level1 sections start
    } else {
        $output = html_writer::tag('div', get_string('nothingtodisplay', 'local_gradebook'), array('class'=>'alert alert-success'));
    }
    
    return $output;
}

function local_gradebook_get_quizzes_list($course, $section, $course_quizzes){
    global $DB, $CFG, $USER;
    $output = '';
    
    if (isset($course_quizzes[$section->id])){
        $i = 1;
        foreach($course_quizzes[$section->id] as $quiz){
            $output .= html_writer::start_tag('tr', array('class'=>'quiz-row'.((count($course_quizzes[$section->id]) == $i) ? ' last-row' : '')));
                $output .= html_writer::tag('td', html_writer::link(new moodle_url('/mod/quiz/view.php',              array('id' => $quiz->cmid)), $quiz->name), array('class'=>'name'));
                $output .= html_writer::tag('td', $quiz->status, array('class'=>'hidden_mobile'));
                $output .= html_writer::tag('td', $quiz->attempts, array('class'=>'hidden_mobile'));
                $output .= html_writer::tag('td', $quiz->grade, array('class'=>'grade'));
            $output .= html_writer::end_tag('tr');
            $i++;
        }
    }
    
    return $output;
}

function local_gradebook_get_course_quizzes($course){
    global $DB, $CFG, $USER;
    
    $items = array();
    $quizzes = $DB->get_records_sql("SELECT q.id, q.name, cm.section, cm.id as cmid, a.userattempts, cmc.completionstate, g.grade as quizgrade 
                                            FROM {course_modules} cm 
                                                LEFT JOIN {modules} m ON m.id = cm.module 
                                                LEFT JOIN {quiz} q ON q.id = cm.instance 
                                                LEFT JOIN (SELECT quiz, COUNT(id) as userattempts FROM {quiz_attempts} WHERE userid = :userid1 GROUP BY quiz) a ON a.quiz = q.id 
                                                LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = :userid2
                                                LEFT JOIN {quiz_grades} g ON g.quiz = q.id AND g.userid = :userid3
                                        WHERE   cm.course = :course 
                                                AND m.name = :modname", 
                                    array('userid1'=>$USER->id, 'userid2'=>$USER->id, 'userid3'=>$USER->id, 'course'=>$course->id, 'modname'=>'quiz'));
    if (count($quizzes)){
        foreach ($quizzes as $quiz){
           
            $quiz->attempts = ($quiz->userattempts) ? $quiz->userattempts : 0;
            $quiz->status = ($quiz->completionstate) ? get_string('complete', 'local_gradebook') : get_string('incomplete', 'local_gradebook');
            
            $mygrade = $quiz->quizgrade;
            $grading_info = grade_get_grades($course->id, 'mod', 'quiz', $quiz->id, $USER->id);
            $grade_item = $grading_info->items[0];
            if (isset($grade_item->grades[$USER->id])) {
                $grade = $grade_item->grades[$USER->id];
                if ($grade->overridden) {
                    $mygrade = $grade->grade;
                }
            }
            $quiz->grade = local_gradebook_format_gradevalue($mygrade, $grade_item);
            
            $items[$quiz->section][$quiz->id] = $quiz;
        }
    }
    
    return $items;
}

/**
 * Returns a percentage representation of a grade value
 *
 * @param float $value The grade value
 * @param object $grade_item Grade item object
 * @param int $decimals The number of decimal places 
 * @return string
 */
function local_gradebook_format_gradevalue($value, $grade_item) {
    if ($value === null){
        return '-';
    }
    
    $min = $grade_item->grademin;
    $max = $grade_item->grademax;
    if ($min == $max) {
        return '-';
    }
    $value = $value + 0;
    $percentage = (($value-$min)*100)/($max-$min);
    return format_float($percentage, 0).'%';
}

function local_gradebook_get_course_sections($course) {
    global $PAGE, $DB;

    $sections = array();
    $allsections = $DB->get_records_sql("SELECT s.*, fs.parent, fs.level, fs.parentssequence FROM {course_sections} s LEFT JOIN {course_format_sections} fs ON fs.sectionid = s.id AND fs.courseid = s.course WHERE s.course = $course->id AND s.section > 0");
    if (count($allsections)){
        foreach($allsections as $section){
            $sections[$section->parent][$section->id] = $section;
        }
    }

    return $sections;
}

